
OC_CMD="oc"

PROJECT=$1

cd $PROJECT

DONE_FOLDER='done'
mkdir $DONE_FOLDER

function ressourcetype_exists() {
  file_number=$(find ./ -name "*.${1}.yaml" | wc -l)
  if [[ $file_number > 0 ]]; then
    return 0
  else
    return 1
  fi
}

# Sort files
for resourcetype in $(echo "rc builds secret serviceaccounts configmap buildconfigs is pvc dc statefulset sts svc routes cronjob storageclass"); do
  ressourcetype_exists $resourcetype && mkdir $resourcetype
  for file in $(ls | grep --color=none $resourcetype.yaml); do mv $file $resourcetype; done
done

# Remove unused files
for resourcetype in $(echo "rc builds storageclass"); do
  [[ -d $resourcetype ]] && mv $resourcetype $DONE_FOLDER
done

# Remove ownerreference (in secret, dc, svc)
for resourcetype in $(echo "configmap dc svc pvc secret"); do
  [[ -d $resourcetype ]] && for file in $(ls $resourcetype); do
    sed -i "/ownerReference/,+5d" $resourcetype/$file;
  done
done

# Services (svc), routes
for resourcetype in $(echo "svc routes"); do $OC_CMD create -f $resourcetype; mv $resourcetype $DONE_FOLDER; done

# Restore configmap, secret, serviceaccounts, buildconfigs and imagestreams
for resourcetype in $(echo "configmap secret serviceaccounts buildconfigs is"); do
  if [[ -d $resourcetype ]]; then
    $OC_CMD create -f $resourcetype
    mv $resourcetype $DONE_FOLDER;
  fi
 done

# Restore PVCs
# TO TEST
if false; then

    # Diff between dynamic and nfs-proxmox storages
    for pvc in $(ls pvc); do
    sed -i "/pv.kubernetes.io\/bind-completed/d" $pvc
    sed -i "/pv.kubernetes.io\/bound-by-controller/d" $pvc
    $OC_CMD create -f pvc/$pvc
    
    # Delete PV claimref
    # Wait until PVC bound
    done
    
    # Restore DBs
    ## $OC_CMD create -f db.dc.yaml
    ## $OC_CMD scale dc/db --replicas=1
    
    # App DC + STS
    
    ## DC Rollout
    
  for resourcetype in $(echo "cronjobs"); do $OC_CMD create -f $resourcetype; mv $resourcetype $DONE_FOLDER; done
    
fi
