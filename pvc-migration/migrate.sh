#!/bin/bash -e

NEW_STORAGECLASS="cephrbd"

NAMESPACE=$1
RESOURCE=$2

if [[ -z $NAMESPACE ]]; then
  echo "Project argument missing"
  exit 1
fi

if [[ -z $RESOURCE ]]; then
  echo "Resource argument missing"
  exit 1
fi

function hold() {
  local seconds="${1:-5}"
  echo "Wait $seconds seconds"
  sleep $seconds
}

POD=$(oc get pods -n $NAMESPACE -l deploymentconfig=$RESOURCE  -o jsonpath='{range .items[?(@.status.phase == "Running")]}{.metadata.name}{end}' | head -n 1)

DBNAME=$(oc -n $NAMESPACE exec $POD -- /bin/bash -c 'echo $MYSQL_DATABASE')
STORAGE=$(oc get -n $NAMESPACE pvc $RESOURCE -o jsonpath='{.spec.resources.requests.storage}')

NEW_PVC=$(cat <<END
{
  "apiVersion": "v1",
  "kind": "PersistentVolumeClaim",
  "metadata": {
    "name": "$RESOURCE",
    "namespace": "$NAMESPACE"
  },
  "spec": {
    "storageClassName": "$NEW_STORAGECLASS",
    "accessModes": ["ReadWriteOnce"],
    "resources": {
      "requests": {
        "storage": "$STORAGE"
      }
    }
  }
}
END
)

echo "Backup $DBNAME"
oc -n $NAMESPACE exec $POD -- /bin/bash -c 'mysqldump -h 127.0.0.1 -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE' > $DBNAME.sql

echo "Scale down the database"
oc scale --replicas=0 -n $NAMESPACE dc $RESOURCE
hold

echo "Delete the pvc"
oc delete pvc -n $NAMESPACE $RESOURCE
hold

echo "Create the PVC with the new storage class"
echo -e $NEW_PVC | oc create -f -

echo "Scale up the database"
oc scale --replicas=1 -n $NAMESPACE dc $RESOURCE
hold 30

echo "Inject the new backup"
POD=$(oc get pods -n $NAMESPACE -l deploymentconfig=$RESOURCE  -o jsonpath='{range .items[?(@.status.phase == "Running")]}{.metadata.name}{end}' | head -n 1)
oc exec -n $NAMESPACE $POD -- /bin/bash -c 'mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE' < $DBNAME.sql

echo 'Finished !'
